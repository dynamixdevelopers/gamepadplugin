# The Game Pad Plugin 
This plugin exposes a game pad gui feature, which can be used to control other things by using the phones motion sensors and buttons on the display.

At this time the game pad sends the keys 37 (left arrow key) if the device is angled to the left,
39 (right arrow key) if the device is angled to the right and 32 (space) if the fire button is pressed.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"Game Pad Plugin",
    "artifact_id":"org.ambientdynamix.contextplugins.gamepadfeature",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[],
    "outputList":{
        "keypress":"SENSOR_KEYPRESS"
    },
    "optionalInputList":[]
}
```